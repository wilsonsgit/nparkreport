window.onload = function () { getContentDiv(window.location.href); }
window.onhashchange = function () { getContentDiv(window.location.href);  }

$(document).ready(function () {
    try{
        $('.dataTable').DataTable({
            "order": [[3, "asc"]],
            aLengthMenu: [
                [10,25, 50, 100, -1],
                [10,25,50, 100, "All"]
            ]
        });
      //  $('.container_div').hide();
    } catch (e) { }
    //try {
        //$('input[type="checkbox"]').bootstrapToggle({
        //    on: 'Yes',
        //    off: 'No',
        //    onstyle: 'success',
        //    offstyle: 'warning'

        //});
    //} catch (e) { }
    //try{
    //    $('.alarm_widget').show("slow");
    //} catch (e) { }
    //try {
    //    $('input[type="checkbox"]').bind("click", function () {
    //        setAttended($(this));
    //    });
    //} catch (e) { }
    try{
        $('#cpcode').multiselect({
            includeSelectAllOption: true,
            selectAllText: 'Select all'
        });
    } catch (e) { }


    $('#publish').on('click', function () {
        publish();
    });
    
});

function openReport(id) {
    alert(id);
}

//function setAttended(checkbox) {
//    var tid = checkbox.data("id");
//    var isSelected = checkbox.is(':checked');
//    var table = checkbox.closest('table').data("id");
//    var uri = window.location.pathname + "/attended";

//    $.ajax({
//        url: uri,
//        type: 'POST',
//        data: JSON.stringify({ task: { code: table, id: tid, selected: isSelected } }),
//        async: false,
//        contentType: 'application/json; charset=utf-8',
//        beforeSend: function () {
//            spinnerMgr(checkbox, true);
//        },
//        success: function (data) {
//            spinnerMgr(checkbox, false);
//        },
//        error: function () {
//           spinnerMgr(checkbox, false);
//        }
//    });
//}

//function spinnerMgr(checkbox,show) {
//    var isSelected = checkbox.is(':checked');
//    var spinr = checkbox.siblings('i');
//    var sort = checkbox.parent('td');

//    if (isSelected) 
//        $(sort).data('order', 1);
//    else
//        $(sort).data('order', 0);


//    if (show) {
//        spinr.removeClass("fa-ck");
//        checkbox.addClass("fa-ck");
//    } else {
//        setTimeout(function () {
//            spinr.addClass("fa-ck");
//            checkbox.removeClass("fa-ck");
//        }, 300);

//    }
//}

//function hideAll() {
//    $('.container_div').hide();
//}

//function getContentDiv(linkTo) {
  
//    hideAll();
//    id = linkTo.split("#");
//    //  $('#' + id[1]).show("fast");
//    $('#' + id[1]).css('display', 'block');
//    initCheckbox('#' + id[1]);
//    currentHash = id[1];
//}

//setTimeout(function () {
//    window.location.reload(1);
//}, 30000);


function publish() {
    var smonth = $('#month').val();
    var syear = $('#year').val();
    var selectedOptions = $('#cpcode option:selected');
    var cplist=""
    selectedOptions.each(function () {
        cplist += $(this).val() + ",";
    });
    
    showHide(true);
    var uri = "/c/Ajax/Publish/";
    $.ajax({
        url: uri,
        type: 'POST',
        data: JSON.stringify({ publish: { month: smonth, year: syear, cpcode: cplist.slice(0, -1) } }),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            showHide(false);
            alert('Publish done.');
            setDLink(data.loc)
        },
        error: function () {
            alert(data.er);
            showHide(false);
        }
    });
}

function setDLink(fname) {
    var link = $('#dl_file');
    link.removeAttr('disabled');
    link.attr("href", "/c/Content/Downloads/"+fname+".xlsx");
}
function showHide(isHide) {
    if (!isHide) {
        $('#publish').text("Publish");
        $('#publish').removeAttr('disabled');
    } else {
        $('#publish').text("Loading");
        $('#publish').attr('disabled', 'disabled');

    }
}
function updateGraph(data) {
    var container = $("#flot-line-chart-moving");
    var maximum = container.outerWidth() / 2 || 300;
    var slist = $('.huge');

    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
    }


    series = [{
        data: res,
        lines: {
            fill: true
        }
    }];

    var plot = $.plot(container, series, {
        grid: {
            borderWidth: 1,
            minBorderMargin: 20,
            labelMargin: 10,
            backgroundColor: {
                colors: ["#fff", "#e4f4f4"]
            },
            margin: {
                top: 8,
                bottom: 20,
                left: 20
            },
            markings: function (axes) {
                var markings = [];
                var xaxis = axes.xaxis;
                for (var x = Math.floor(xaxis.min) ; x < xaxis.max; x += xaxis.tickSize * 2) {
                    markings.push({
                        xaxis: {
                            from: x,
                            to: x + xaxis.tickSize
                        },
                        color: "rgba(232, 232, 255, 0.2)"
                    });
                }
                return markings;
            }
        },
        xaxis: {
            tickFormatter: function () {
                return "";
            }
        },
        yaxis: {
            min: 0,
            max: max
        },
        legend: {
            show: true
        }
    });

    series[0].data = res;
    plot.setData(series);
    plot.draw();
}