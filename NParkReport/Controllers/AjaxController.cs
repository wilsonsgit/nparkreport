﻿using Microsoft.Win32;
using NParkReport.DAO;
using NParkReport.Models;
using NParkReport.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;


namespace NParkReport.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Publish(Publish publish)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

                var cp_list = publish.cpcode.Split(',');
                List<Carpark> cplist = (List<Carpark>)Session["cplist"];
                Util util = new Util();

                int sheetNumber = 1;
                foreach (String cp in cp_list)
                {
                    List<Report> reportList = new DBConnection().getReport(cp, Int32.Parse(publish.month), Int32.Parse(publish.year));
                    try
                    {
                        worksheet = workbook.Sheets[sheetNumber];
                        worksheet.Name = cp;
                        int cellRowIndex = 4;
                        double total_amount = 0;

                        //Header 1
                        worksheet.Cells[1, 1] = String.Format("{0} - Free Parking", util.searchName(cplist, cp));
                        //Header 2
                        worksheet.Cells[2, 1] = String.Format("{0} {1}", new DateTime(Int32.Parse(publish.year), Int32.Parse(publish.month), 1).ToString("MMMM", CultureInfo.InvariantCulture), publish.year);
                        //Header 3
                        worksheet.Cells[3, 1] = "IU/Autopass Card";
                        worksheet.Cells[3, 2] = "Paid Cashcard No.";
                        worksheet.Cells[3, 3] = "Vehicle Type";
                        worksheet.Cells[3, 4] = "Entry Time";
                        worksheet.Cells[3, 5] = "Exit Time";
                        worksheet.Cells[3, 6] = "Parking Vehicle";
                        worksheet.Cells[3, 7] = "Duration";
                        worksheet.Cells[3, 8] = "WPS/Mins";
                        worksheet.Cells[3, 9] = "Amount";

                        foreach (Report report in reportList)
                        {
                            worksheet.Cells[cellRowIndex, 1] = report.IU;
                            worksheet.Cells[cellRowIndex, 2] = report.CAN;
                            worksheet.Cells[cellRowIndex, 3] = report.vhtype;
                            worksheet.Cells[cellRowIndex, 4] = report.etime;
                            worksheet.Cells[cellRowIndex, 5] = report.xtime;
                            worksheet.Cells[cellRowIndex, 6] = report.cdtype;
                            worksheet.Cells[cellRowIndex, 7] = report.duration;
                            worksheet.Cells[cellRowIndex, 8] = report.wpsmins;
                            worksheet.Cells[cellRowIndex, 9] = report.amount;
                            total_amount += Double.Parse(report.amount);
                            cellRowIndex++;
                        }
                        worksheet.Cells[cellRowIndex, 9] = total_amount;
                        workbook.Sheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
                    }
                    catch (System.Exception ex)
                    {
                        return Json(new { success = false, er = ex.Message });
                    }
                    sheetNumber++;
                }

                try
                {
                    //@"C:\test.xlsx"
                    String savLoc = util.savePath();
                    workbook.SaveAs(savLoc);
                    workbook.Close();
                    excel.Quit();
                    GC.Collect();

                    Marshal.ReleaseComObject(workbook);
                    Marshal.ReleaseComObject(excel);

                    return Json(new { success = true, loc = Path.GetFileNameWithoutExtension(savLoc)});
                }
                catch (System.Exception ex)
                {
                    return Json(new { success = false, er = ex.Message });
                }

            }
            catch (System.Exception ex)
            {
                return Json(new { success = false, er = ex.Message });
            }
        }

    }
}
