﻿using NParkReport.Models;
using NParkReport.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NParkReport.Controllers
{
    public class HomeController : Controller
    {
        private Util util;
        // GET: /
        public ActionResult Index(String id)
        {

            ViewBag.Title = "Home";
            if (String.IsNullOrEmpty(id))
                return View();
            else
                return ShowView(id);
                
        }
        // GET: /{id}/
        public ActionResult ShowView(String id)
        {
            List<Carpark> cplist = (List<Carpark>)Session["cplist"];
            util = new Util();

            ViewBag.Title = util.searchName(cplist, id);
            ViewBag.Code = id;
            try
            {
                if (Request.Form["month"] != null && Request.Form["year"] != null)
                {
                    ViewBag.fmonth = Int32.Parse(Request.Form["month"]);
                    ViewBag.fyear = Int32.Parse(Request.Form["year"]);
                    Session["month"] = ViewBag.fmonth;
                    Session["year"] = ViewBag.fyear;
                }
                else
                {
                    if (Session["month"] != null && Session["year"] != null)
                    {
                        ViewBag.fmonth = Int32.Parse(Session["month"].ToString());
                        ViewBag.fyear = Int32.Parse(Session["year"].ToString());
                    }
                    else
                    {
                        ViewBag.fmonth = null;
                        ViewBag.fyear = null;
                    }
                }
            }
            catch (Exception)
            {
                ViewBag.fmonth = null;
                ViewBag.fyear = null;
            }
   
            ViewBag.yearList = new SelectList(new Util().year(), "Value", "Text", ViewBag.fyear);
            ViewBag.monthList = new SelectList(new Util().month(), "Value", "Text", ViewBag.fmonth);


            if (ViewBag.fyear != null && ViewBag.fmonth != null)
            {
                var model = new Report
                {
                    year = (string)ViewBag.fyear.ToString(),
                    month = (string)ViewBag.fmonth.ToString()

                };
                return View("ShowView", model);
            }else
                return View("ShowView");
           
        }
        [HttpPost]
        public ActionResult Publish(Publish publish)
        {
            return Json(new { success = true });
        }
    }
}
