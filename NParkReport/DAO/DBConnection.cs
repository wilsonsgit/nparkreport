﻿using NParkReport.Models;
using NParkReport.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NParkReport.DAO
{

    public class DBConnection
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection = new SqlConnection(sConString);


        public List<Carpark> getCarparks()
        {
            List<Carpark> cplist = new List<Carpark>();

            String sSQL = " SELECT CPNAME,CPCODE,CPIP FROM [CarParkCentral].[dbo].[CarParkVPNList]   " +
                            " WHERE CPCODE IN (                                                       " +
                            " 	SELECT CPCODE                                                         " +
                            " 	FROM [CarParkCentral].[dbo].[CarParkDivision]                         " +
                            " 	WHERE SUBDIVISION= 'NPARKS'                                           " +
                            " 	AND DIVISION <> 'MANAGED'                                             " +
                            " )                                                                       " +
                            " AND ACTIVE = 1                                                          " +
                            " ORDER BY CPCODE                                                         ";
            
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;
            cmd.CommandText = sSQL;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            Util util = new Util();
            while (dReader.Read())
            {
                Carpark cp = new Carpark();
                cp.code = dReader["CPCODE"].ToString();
                cp.ip = dReader["CPIP"].ToString();
                cp.name =  util.parseCPName(dReader["CPNAME"].ToString());
                cplist.Add(cp);
            }
            return cplist;

        }
        public List<Report> getReport(string cpcode,int month, int year)
        {
            var firstDayOfMonth = new DateTime(year, month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var startdate = String.Format("{0}-{1}-{2}", year, month, firstDayOfMonth.ToString("dd"));
            var enddate = String.Format("{0}-{1}-{2}", year, month, lastDayOfMonth.ToString("dd"));

            String sSQL =   " DECLARE                                                                                                                                                                                                                                " +
                            " @STARTDATE VARCHAR(20), @ENDDATE VARCHAR(20), @WPS_STIME VARCHAR(20), @WPS_ETIME VARCHAR(20),                                                                                                                                          " +
                            " @SHARE1 VARCHAR(20), @SHARE2 VARCHAR(20), @WPS_DURATION INTEGER, @FREE_PARK_DURATION INTEGER                                                                                                                                           " +
                            " SET @STARTDATE = '" + startdate + "'                                                                                                                                                                                                   " +
                            " SET @ENDDATE = '" + enddate + "'                                                                                                                                                                                                            " +
                            " SET @WPS_STIME = '06:30:00.000'                                                                                                                                                                                                        " +
                            " SET @WPS_ETIME = '08:30:00.000'                                                                                                                                                                                                        " +
                            " SET @WPS_DURATION = 120                                                                                                                                                                                                                " +
                            " SET @FREE_PARK_DURATION = 15                                                                                                                                                                                                           " +
                            "                                                                                                                                                                                                                                        " +
                            " ;WITH [STEP_1] as (                                                                                                                                                                                                                    " +
                            " 	SELECT *                                                                                                                                                                                                                             " +
                            " 	FROM [Transaction_Central].[dbo].[" + cpcode + "_Trans]                                                                                                                                                                                         " +
                            " 	WHERE                                                                                                                                                                                                                                " +
                            " 	DATE1 BETWEEN @STARTDATE AND @ENDDATE                                                                                                                                                                                                " +
                            " 	AND CARTYPE NOT IN(2,230)                                                                                                                                                                                                            " +
                            " 	AND CARDTYPE = 0                                                                                                                                                                                                                     " +
                            " 	AND STATE = 1                                                                                                                                                                                                                        " +
                            " ), [STEP_2] as(                                                                                                                                                                                                                        " +
                            " 	SELECT * FROM (                                                                                                                                                                                                                      " +
                            " 		SELECT *,                                                                                                                                                                                                                        " +
                            " 		CASE                                                                                                                                                                                                                             " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(TIME1 AS DATE), CAST(LASTENTRYTIME AS DATE) ) =  0) AND                                                                                                                                          " +
                            " 					LASTENTRYTIME >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME) AND                                                                                                                                                       " +
                            " 					LASTENTRYTIME <= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME) AND                                                                                                                                                       " +
                            " 					TIME1 <= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)                                                                                                                                                                   " +
                            " 				 )                                                                                                                                                                                                                       " +
                            " 			THEN '0'                                                                                                                                                                                                                     " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(TIME1 AS DATE), CAST(LASTENTRYTIME AS DATE) ) =  0) AND                                                                                                                                          " +
                            " 					LASTENTRYTIME >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME) AND                                                                                                                                                       " +
                            " 					LASTENTRYTIME <= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME) AND                                                                                                                                                       " +
                            " 					TIME1 > (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)                                                                                                                                                                    " +
                            " 				 )	                                                                                                                                                                                                                     " +
                            " 			THEN '1'                                                                                                                                                                                                                     " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(TIME1 AS DATE), CAST(LASTENTRYTIME AS DATE) ) =  0) AND                                                                                                                                          " +
                            " 					TIME1 >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME) AND                                                                                                                                                               " +
                            " 					TIME1 <= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)                                                                                                                                                                   " +
                            " 				 )	                                                                                                                                                                                                                     " +
                            " 			THEN '2'                                                                                                                                                                                                                     " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(TIME1 AS DATE), CAST(LASTENTRYTIME AS DATE) ) =  0) AND                                                                                                                                          " +
                            " 					LASTENTRYTIME <= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME) AND                                                                                                                                                       " +
                            " 					TIME1 >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME)                                                                                                                                                                   " +
                            " 				 )	                                                                                                                                                                                                                     " +
                            " 			THEN '3'                                                                                                                                                                                                                     " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(LASTENTRYTIME AS DATE),CAST(TIME1 AS DATE) ) =  1) AND                                                                                                                                           " +
                            " 					TIME1 >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME)                                                                                                                                                                   " +
                            " 				 )	                                                                                                                                                                                                                     " +
                            " 			THEN '4'                                                                                                                                                                                                                     " +
                            " 			WHEN                                                                                                                                                                                                                         " +
                            " 				(                                                                                                                                                                                                                        " +
                            " 					(DATEDIFF(DAY, CAST(LASTENTRYTIME AS DATE),CAST(TIME1 AS DATE) ) >  1)                                                                                                                                               " +
                            " 				 )	                                                                                                                                                                                                                     " +
                            " 			THEN '5'                                                                                                                                                                                                                     " +
                            " 			ELSE NULL                                                                                                                                                                                                                    " +
                            " 		END AS SHARE                                                                                                                                                                                                                     " +
                            " 		FROM STEP_1                                                                                                                                                                                                                      " +
                            " 	)S                                                                                                                                                                                                                                   " +
                            " 	WHERE S.SHARE IS NOT NULL                                                                                                                                                                                                            " +
                            " ),[STEP_3_A] as(                                                                                                                                                                                                                       " +
                            " 	SELECT *,                                                                                                                                                                                                                            " +
                            " 		DATEDIFF(S, LASTENTRYTIME, TIME1) / CAST(60 AS DECIMAL (36,2)) AS DURATION                                                                                                                                                       " +
                            " 	FROM                                                                                                                                                                                                                                 " +
                            " 	[STEP_2]                                                                                                                                                                                                                             " +
                            " ),[STEP_3_B] as (                                                                                                                                                                                                                      " +
                            " 	SELECT *,                                                                                                                                                                                                                            " +
                            " 		CASE                                                                                                                                                                                                                             " +
                            " 		WHEN SHARE = 0 THEN DURATION                                                                                                                                                                                                     " +
                            " 		WHEN SHARE = 1 THEN DATEDIFF(S,LASTENTRYTIME,(CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)) / CAST(60 AS DECIMAL (36,0))                                                                                                             " +
                            " 		WHEN SHARE = 2 THEN DATEDIFF(S,(CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME), TIME1) / CAST(60 AS DECIMAL (36,0))                                                                                                                    " +
                            " 		WHEN SHARE = 3 THEN @WPS_DURATION                                                                                                                                                                                                " +
                            " 		WHEN SHARE = 4 THEN                                                                                                                                                                                                              " +
                            " 				CASE WHEN CAST(TIME1 AS DATETIME) >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)                                                                                                                                           " +
                            " 					THEN @WPS_DURATION                                                                                                                                                                                                   " +
                            " 				ELSE ((DATEDIFF(S,(CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME),CAST(TIME1 AS DATETIME)))) / CAST(60 AS DECIMAL (36,0))                                                                                                      " +
                            " 				END                                                                                                                                                                                                                      " +
                            " 		WHEN SHARE = 5 THEN                                                                                                                                                                                                              " +
                            " 				CASE                                                                                                                                                                                                                     " +
                            " 					WHEN CAST(TIME1 AS DATETIME) >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_ETIME)                                                                                                                                            " +
                            " 						THEN @WPS_DURATION * ((DATEDIFF(D,CAST(LASTENTRYTIME AS DATE),CAST(TIME1 AS DATE))))                                                                                                                             " +
                            " 					WHEN CAST(TIME1 AS DATETIME) >= (CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME)                                                                                                                                            " +
                            " 						THEN ((DATEDIFF(S,(CONVERT(VARCHAR,[DATE1])+' '+@WPS_STIME),CAST(TIME1 AS DATETIME))))/CAST(60 AS DECIMAL (36,0)) + ((DATEDIFF(D,CAST(LASTENTRYTIME AS DATE),CAST(TIME1 AS DATE))-1) * @WPS_DURATION)            " +
                            " 					ELSE @WPS_DURATION * (((DATEDIFF(D,CAST(LASTENTRYTIME AS DATE),CAST(TIME1 AS DATE))))-1)                                                                                                                             " +
                            " 				END                                                                                                                                                                                                                      " +
                            " 		END AS  WPS_MINS	                                                                                                                                                                                                             " +
                            " 	FROM [STEP_3_A]                                                                                                                                                                                                                      " +
                            " ), [STEP_3_C] as (                                                                                                                                                                                                                     " +
                            " SELECT *,                                                                                                                                                                                                                              " +
                            " CASE                                                                                                                                                                                                                                   " +
                            " 		WHEN DURATION <= @FREE_PARK_DURATION THEN 0                                                                                                                                                                                      " +
                            " 		ELSE CAST( (WPS_MINS / 30) * 0.5 AS NUMERIC(36,2))                                                                                                                                                                               " +
                            " 		END AS AMOUNT_1	                                                                                                                                                                                                                 " +
                            " FROM STEP_3_B                                                                                                                                                                                                                          " +
                            " ), [FINAL] as(                                                                                                                                                                                                                         " +
                            " 	SELECT *                                                                                                                                                                                                                             " +
                            " 	FROM STEP_3_C                                                                                                                                                                                                                        " +
                            " 	WHERE AMOUNT_1 <> 0                                                                                                                                                                                                                  " +
                            "                                                                                                                                                                                                                                        " +
                            " )                                                                                                                                                                                                                                      " +
                            " SELECT                                                                                                                                                                                                                                 " +
                            " 	CARID,                                                                                                                                                                                                                               " +
                            " 	CASE WHEN CAN = '0000000000000000' THEN '' ELSE CAN END AS 'CAN',                                                                                                                                                                    " +
                            " 	CASE                                                                                                                                                                                                                                 " +
                            " 		WHEN CARTYPE = 0 THEN 'Car/Van'                                                                                                                                                                                                  " +
                            " 		WHEN CARTYPE = 1 THEN 'Lorry'                                                                                                                                                                                                    " +
                            " 		WHEN CARTYPE = 3 THEN 'Sidecar'                                                                                                                                                                                                  " +
                            " 	END AS 'CARTYPE',                                                                                                                                                                                                                    " +
                            " 	LASTENTRYTIME AS 'ENTRY_TIME',                                                                                                                                                                                                       " +
                            " 	TIME1 AS 'EXIT_TIME',                                                                                                                                                                                                                " +
                            " 	CASE WHEN CARDTYPE = 0 THEN 'Hourly' END AS 'CARDTYPE',                                                                                                                                                                              " +
                            " 	CONVERT(varchar(8), DATEADD(SECOND, DATEDIFF(SECOND, LASTENTRYTIME, TIME1), 0), 114) AS 'DURATION',                                                                                                                                  " +
                            " 	ROUND(CONVERT(NUMERIC(36,0),DURATION),0) AS 'MINS',                                                                                                                                                                                  " +
                            " 	ROUND(CONVERT(NUMERIC(36,0),WPS_MINS),0) AS 'WPS_MINS',                                                                                                                                                                              " +
                            " 	AMOUNT_1 AS 'AMOUNT'                                                                                                                                                                                                                 " +
                            " FROM [FINAL]                                                                                                                                                                                                                           " +
                            " ORDER BY CAST(LASTENTRYTIME AS DATETIME)                                                                                                                                                                                               " ;




            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;
            cmd.CommandText = sSQL;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            Util util = new Util();
            List<Report> list = new List<Report>();

            while (dReader.Read())
            {
                Report report = new Report();
                report.amount = dReader["AMOUNT"].ToString();
                report.CAN = dReader["CAN"].ToString();
                report.cdtype = dReader["CARDTYPE"].ToString();
                report.duration = dReader["DURATION"].ToString();
                report.etime = dReader["ENTRY_TIME"].ToString();
                report.inmins = dReader["MINS"].ToString();
                report.IU = dReader["CARID"].ToString();
                report.vhtype = dReader["CARTYPE"].ToString();
                report.wpsmins = dReader["WPS_MINS"].ToString();
                report.xtime = dReader["EXIT_TIME"].ToString();
                list.Add(report);
            }
            return list;
        }
    }
}