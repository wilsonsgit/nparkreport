﻿using Microsoft.Win32;
using NParkReport.DAO;
using NParkReport.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NParkReport.Utility
{
    public class Util
    {

        public string []months = {"January"};
        public Util(){}

        public String parseCPName(String str)
        {
            String temp = str.Replace("(", String.Empty);
            temp = temp.Replace(")", String.Empty);
            String []temp2 = temp.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            String final = "";
            for (int i = 2; i < temp2.Length; i++)
            {
                final += " " + temp2[i];
            }
            return final;
        }

        public String searchName(List<Carpark> cplist, String id)
        {
            foreach (Carpark cp in cplist)
            {
                if (cp.code == id)
                    return cp.name;
            }
            return null;
        }

        public List<ListItem> month()
        {
            var listItems = new List<ListItem> 
            { 
                  new ListItem { Text = "January", Value = "1"}, 
                  new ListItem { Text = "February", Value = "2" }, 
                  new ListItem { Text = "March", Value = "3"}, 
                  new ListItem { Text = "April", Value = "4" }, 
                  new ListItem { Text = "May", Value = "5" }, 
                  new ListItem { Text = "June", Value = "6" }, 
                  new ListItem { Text = "July", Value = "7" }, 
                  new ListItem { Text = "August", Value = "8" }, 
                  new ListItem { Text = "September", Value = "9"}, 
                  new ListItem { Text = "October", Value = "10" }, 
                  new ListItem { Text = "November", Value = "11" }, 
                  new ListItem { Text = "December", Value = "12" }
            };

            return listItems;
        }

        public List<ListItem> year()
        {

            int presentYear = DateTime.Now.Year;
            int startYear = 2016;
            var listItems = new List<ListItem>();

            for (int i = startYear; i <= presentYear; i++)
            {
                listItems.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return listItems;
        }

        public List<ListItem> cpcode()
        {
            var listItems = new List<ListItem>();

            foreach (Carpark cp in new DBConnection().getCarparks())
            {
                listItems.Add(new ListItem { Text = cp.name, Value = cp.code });
            }
            return listItems;
        }


        public String savePath()
        {
            String path = String.Empty;
            path = HttpContext.Current.Server.MapPath("~/Content/Downloads");
            return path +"/" + fn();
        }

        public string fn()
        {
            DateTime localDate = DateTime.Now;
            String date = localDate.ToString(new CultureInfo("en-GB"));
            date = date.Replace("/", String.Empty);
            date = date.Replace(":", String.Empty);
            date = date.Replace(" ", String.Empty);
            return date+".xlsx";
        }


    }
}