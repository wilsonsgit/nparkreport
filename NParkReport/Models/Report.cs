﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NParkReport.Models
{
    public class Report
    {
        public string month { get; set; }
        public string year { get; set; }
        public string IU { get; set; }
        public string CAN { get; set; }
        public string vhtype { get; set; }
        public string etime { get; set; }
        public string xtime { get; set; }
        public string cdtype { get; set; }
        public string duration { get; set; }
        public string inmins { get; set; }
        public string wpsmins { get; set; }
        public string amount { get; set; }
    }
}