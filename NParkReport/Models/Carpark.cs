﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NParkReport.Models
{
    public class Carpark
    {
        public String name { get; set; }
        public String code { get; set; }
        public String ip { get; set; }
    }
}